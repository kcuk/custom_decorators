import { Component, OnInit } from '@angular/core';

function funcU(parameter_passed_to_decorator){
  return function (function_return_the_parameter_where_this_decorator_is_called){//it is called the callback (ie function inside the function)
    function_return_the_parameter_where_this_decorator_is_called.prototype.varAny=parameter_passed_to_decorator.decorator_used_variable;
  } 
}


@funcU({ decorator_used_variable:['kc','ab']})
export class FuncClass{

}

@Component({
  selector: 'app-decorator',
  templateUrl: './decorator.component.html',
  styleUrls: ['./decorator.component.css']
})
export class DecoratorComponent implements OnInit {

  constructor() {
 
   }

  ngOnInit() {

  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//prime ng support
import {ToastModule} from 'primeng/toast';

//angular material support
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import { DecoratorComponent } from './decorator/decorator.component';




@NgModule({
  declarations: [
    AppComponent,
    DecoratorComponent
  ],
  imports: [
    MatButtonModule,
    MatTabsModule,
    ToastModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule { }
